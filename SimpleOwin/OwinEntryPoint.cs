﻿using System.Diagnostics;
using Microsoft.Owin;
using Owin;
using SimpleOwin;


[assembly: OwinStartup(typeof(OwinEntryPoint), nameof(OwinEntryPoint.CustomConfiguration))]

namespace SimpleOwin
{

    public class OwinEntryPoint
    {
        public static void CustomConfiguration(IAppBuilder app)
        {
            app.Use(async (ctx, next) =>
            {
                Debug.WriteLine($"Incoming request: {ctx.Request.Path}");
                await next().ConfigureAwait(false);
                Debug.WriteLine($"Outgoing request: {ctx.Request.Path}");

            });
            app.Use(async (ctx, next) =>
            {
                await ctx.Response.WriteAsync("World").ConfigureAwait(false);
            });
        }
    }
}