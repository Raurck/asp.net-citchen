﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using Owin.MiddleWare;

namespace OwinSelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebApp.Start<Owin.MiddleWare.Startup>("http://localhost:12345"))
            {
                Console.ReadLine();
            }
        }
    }
}
