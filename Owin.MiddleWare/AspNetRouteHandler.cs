﻿using System.Web;
using System.Web.Routing;

namespace Owin.MiddleWare
{
    internal class AspNetRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new AspNetHandler();
        }
    }
}