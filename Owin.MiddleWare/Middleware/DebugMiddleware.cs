﻿using System.Collections.Generic;
using System.Diagnostics;
using System.EnterpriseServices;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin.MiddleWare.Middleware;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

namespace Owin.MiddleWare.Middleware
{
    public class DebugMiddleware
    {
        private readonly  AppFunc _next;
        private readonly DebugMiddlewareOptions _options;
        public DebugMiddleware(AppFunc next, DebugMiddlewareOptions options = null)
        {
            _next = next;
            _options = options ?? new DebugMiddlewareOptions();

            if (_options.OnIncomingRequest == null)
                _options.OnIncomingRequest = (ctx) => { Debug.WriteLine($"Incoming request: {ctx.Request.Path}"); };

            if (_options.OnOutgoingRequest == null)
                _options.OnOutgoingRequest = (ctx) => { Debug.WriteLine($"Outgoing request: {ctx.Request.Path}"); };


        }

        public async  Task Invoke(IDictionary<string, object> environment)
        {
            var ctx = new OwinContext(environment);
            //var path  = (string) environment["Owin.RequestPath"];
            _options.OnIncomingRequest(ctx);
            await _next(environment).ConfigureAwait(false);
            _options.OnOutgoingRequest(ctx);
        }
    }
}