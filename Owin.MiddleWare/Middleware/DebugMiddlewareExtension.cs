﻿namespace Owin.MiddleWare.Middleware
{
    public static class DebugMiddlewareExtension
    {
        public static IAppBuilder UseDebugMiddleware(this IAppBuilder app, DebugMiddlewareOptions options = null)
        {
            app.Use<DebugMiddleware>(options);
            return app;
        }
    }
}