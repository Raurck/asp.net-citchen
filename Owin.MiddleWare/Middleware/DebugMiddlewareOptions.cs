﻿using System;
using Microsoft.Owin;

namespace Owin
{
    public class DebugMiddlewareOptions
    {
        public Action<IOwinContext> OnIncomingRequest  { get; set; }
        public Action<IOwinContext> OnOutgoingRequest { get; set; }
    }
}