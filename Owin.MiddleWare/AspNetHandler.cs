﻿using System.Web;

namespace Owin.MiddleWare
{
    internal class AspNetHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Write("Handle by AspNet");
        }

        public bool IsReusable => false;
    }
}