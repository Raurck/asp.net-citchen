﻿using System.Diagnostics;
using System.Runtime.InteropServices.ComTypes;
using System.Web.DynamicData;
using System.Web.Http;
using System.Web.Routing;
using Microsoft.Owin.Builder;
using Microsoft.Owin.BuilderProperties;
using Owin.MiddleWare.Middleware;


namespace Owin.MiddleWare
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            RouteTable.Routes.Add(new Route("aspnet", new AspNetRouteHandler()));
            //RouteTable.Routes.MapOwinRoute("owin", "owin", OwinEntryPoint.CustomConfiguration);

            //app.UseStaticFiles();
            
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            app.UseWebApi(config);
            
            var properties = new AppProperties(app.Properties);
            
            app.UseDebugMiddleware(new DebugMiddlewareOptions
                {
                    OnIncomingRequest = ctx =>
                    {
                        var watch = new Stopwatch();
                        watch.Start();
                        ctx.Environment["DebugStopWatch"] = watch;
                    },
                    OnOutgoingRequest = ctx =>
                    {
                        var watch = (Stopwatch) ctx.Environment["DebugStopWatch"];
                        watch.Stop();
                        Debug.WriteLine($"Request execution time: {watch.ElapsedMilliseconds} ms");
                    }
                }
                );

            app.Use(async (ctx, next) =>
            {
                if(ctx.Request.Path.Value == "/")
                    await ctx.Response.WriteAsync("Test 2").ConfigureAwait(false);
                await next().ConfigureAwait(false);
            });

        }
    }
}