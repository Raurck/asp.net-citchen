﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Owin.MiddleWare.Controllers
{
    [RoutePrefix("api")]
    public class HelloWorldController : ApiController
    {
        [Route("index")]
        [HttpGet]
        public async Task<IHttpActionResult> Index()
        {
            return Content(HttpStatusCode.OK, "Hello from WebAPI");
        }
    }
}
